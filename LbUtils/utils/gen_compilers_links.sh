#!/bin/bash -e

destdir=$(cd $(dirname $0)/../scripts && pwd)

for cmd in g{++,cc,fortran} c++ \
           clang{,++,-apply-replacements,-check,-format,-include-fixer,-modernize,-query,-rename,-tidy} ; do
    for prefix in {/cvmfs/sft.cern.ch,/afs/cern.ch/sw}/lcg/{contrib,external,releases,experimental}/{gcc,llvm} ; do
        for platform in {i686,x86_64}-{slc5,slc6,cc7,centos7}{,-gcc49-opt,-gcc62-opt} ; do
            for found in ${prefix}/*/${platform}/bin/${cmd} ; do
                # extract 3 digits version for gcc and 2 or 3 digits for clang (llvm)
                version=$(echo "$found" | sed -r -n 's_.*/(gcc/([0-9]+\.[0-9]+\.[0-9]+)|llvm/([0-9]+\.[0-9]+(\.[0-9]+)?))/.*_\2\3_p')
                if [ -n "$version" ] ; then
                    dst=lcg-${cmd}-${version}
                    if [ ! -e ${destdir}/${dst} ] ; then
                      ln -sv .lcg-compiler-wrapper.sh ${destdir}/${dst}
                    fi
                fi
            done
        done
    done
done
